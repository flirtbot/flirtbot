# FLIRTBOT #
#### The open source version of Flirtbot by @nerdpositive and @FluffyPira. ####
#### Open Source version hosted at: <https://bitbucket.org/flirtbot/flirtbot> ####
#### Public version at: <http://waa.ai/flirt> ####

### About ###
---------------------------------------
*FLIRTBOT* is a bot created by Kelly Harper (@nerdpositive) from a crazy joint idea with Pira (@FluffyPira) to make a bot that randomly generates flirts. 

Please see the changelog in Changelog.md

### Tech Details ###
---------------------------------------
FLIRTBOT uses various lists in a .TXT format to generate the flirts. The public version of FLIRTBOT uses three variants of "flirts". Those are X-type, Y-type, and Z-type. Users are first sent to *rand.php* which randomly chooses one of the pages via a simple Javascript. 

> Note that rand.php and the XYZ combinations are included in the source under their respective file names.

### Contact Info ###
---------------------------------------
If you need ANY help at all with FLIRTBOT, email her flirtiness at: flirtbot@cutey.com

If you need assistance with web deployment, have a suggestion for the PHP files, or anything else with the PHP assets please tweet @nerdpositive.

If you need help with the Ruby, the Twitter intergration, or anything else along the lines of that, I bet @FluffyPira can help you with that.

### License ###
---------------------------------------
The MIT License (MIT)

Copyright (c) 2014 Kelly Harper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.