#!/usr/bin/env ruby

require 'twitter_ebooks'
require 'pismo'
include Ebooks

# this bot is based largely on a mispy bot because I wanted it to have mispy responses. Most of the "features" don't even work like the model and are just there because of laziness.

CONSUMER_KEY = ""
CONSUMER_SECRET = ""
DELAY = 2..30 # Simulated human reply delay range in seconds
BLACKLIST = ['insomnius', 'upulie', 'horse_inky', 'gray_noise', 'lookingglasssab', 'AmaznPhilEbooks'] # Grumpy users to avoid interaction with

# Track who we've randomly interacted with globally
$have_talked = {}

class GenBot
  def initialize(bot, modelname)
    @bot = bot
    @model = nil

    bot.consumer_key = CONSUMER_KEY
    bot.consumer_secret = CONSUMER_SECRET

    bot.on_startup do
      @model = Model.load("model/#{modelname}.model")
      @top100 = @model.keywords.top(100).map(&:to_s).map(&:downcase)
      @top50 = @model.keywords.top(20).map(&:to_s).map(&:downcase)      
    end

    bot.on_message do |dm|
      
      noun = File.open("nouns.txt").readlines.shuffle.pop
      noun.delete!("\n")
      adj = File.open("adj.txt").readlines.shuffle.pop
      adj.delete!("\n")
      prefix = File.open("prefix.txt").readlines.shuffle.pop
      prefix.delete!("\n") 
      
      bot.delay DELAY do
        bot.reply dm, "#{prefix} #{adj} #{noun}"
      end
    end

    bot.on_follow do |user|
      bot.delay DELAY do
        bot.follow user[:screen_name]
      end
    end

    bot.on_mention do |tweet, meta|
      # Avoid infinite reply chains (very small chance of crosstalk)
      next if tweet[:user][:screen_name].include?('ebooks') && rand > 0.20
      next if tweet[:user][:screen_name].include?('bot') && rand > 0.20
      next if tweet[:user][:screen_name].include?('generateacat') && rand > 0.10
      next if tweet[:user][:screen_name].include?('GenerateACat') && rand > 0.10
      
      reply(tweet, meta)
    end
    
    bot.on_timeline do |tweet, meta|
      next if tweet[:retweeted_status] || tweet[:text].start_with?('RT')
      next if BLACKLIST.include?(tweet[:user][:screen_name])
      
      tokens = NLP.tokenize(tweet[:text])

      # We calculate unprompted interaction probability by how well a
      # tweet matches our keywords
      interesting = tokens.find { |t| @top100.include?(t.downcase) }
      very_interesting = tokens.find_all { |t| @top50.include?(t.downcase) }.length > 2
      special = tokens.find { |t| ['bot', 'bots', 'ebooks', 'flirt', 'sex', 'kelly', 'pira', 'banana', 'penis', 'vagina', 'butt', 'sex', 'fuck'].include?(t) }

      if special
        favorite(tweet) if rand < 0.5
        favd = true # Mark this tweet as favorited

        #bot.delay DELAY do
        #  bot.follow tweet[:user][:screen_name]
        #end
      end

      # Any given user will receive at most one random interaction per day
      # (barring special cases)
      next if $have_talked[tweet[:user][:screen_name]]
      $have_talked[tweet[:user][:screen_name]] = true

      if very_interesting || special
        favorite(tweet) if (rand < 0.40 && !favd) # Don't fav the tweet if we did earlier
        retweet(tweet) if rand < 0.25
        reply(tweet, meta) if rand < 0.10
      elsif interesting
        favorite(tweet) if rand < 0.20
        reply(tweet, meta) if rand < 0.10
      end
    end
    
    # Schedule a main tweet for every 49m
    bot.scheduler.every '2650' do
      
      noun = File.open("nouns.txt").readlines.shuffle.pop
      noun.delete!("\n")
      adj = File.open("adj.txt").readlines.shuffle.pop
      adj.delete!("\n")
      prefix = File.open("prefix.txt").readlines.shuffle.pop
      prefix.delete!("\n")  
      
      bot.tweet("#{prefix} #{adj} #{noun}.") 
      
#      @randvar = Random.rand (1..4)
#      This stays commented for now until we come up with cases where we can mix and match random flirt elements or flirt "Types" similar to how the website has x,y,z. This will be impliment VERY VERY soon
#      if @randvar == 1
#        bot.tweet("#{prefix} #{adj} #{noun}.")          
#      elsif @randvar == 2
#      elsif @randvar == 3 
#      elsif @randvar == 4
#      end
    end
  end
  
  def reply(tweet, meta)
    noun = File.open("nouns.txt").readlines.shuffle.pop
    noun.delete!("\n")
    adj = File.open("adj.txt").readlines.shuffle.pop
    adj.delete!("\n")
    prefix = File.open("prefix.txt").readlines.shuffle.pop
    prefix.delete!("\n") 
    
    @bot.delay DELAY do
      @bot.reply tweet, meta[:reply_prefix] + "#{prefix} #{adj} #{noun}."
      # this will likely follow the same behvior very VERY shortly.
    end
  end

  def favorite(tweet)
    @bot.log "Favoriting @#{tweet[:user][:screen_name]}: #{tweet[:text]}"
    @bot.delay DELAY do
      @bot.twitter.favorite(tweet[:id])
    end
  end

  def retweet(tweet)
    @bot.log "Retweeting @#{tweet[:user][:screen_name]}: #{tweet[:text]}"
    @bot.delay DELAY do
      @bot.twitter.retweet(tweet[:id])
    end
  end
end

def make_bot(bot, modelname)
  GenBot.new(bot, modelname)
end

Ebooks::Bot.new("flirt_bot") do |bot| # Ebooks account username
  bot.oauth_token = "" # oauth token for ebooks account
  bot.oauth_token_secret = "" # oauth secret for ebooks account

  make_bot(bot, "flirtbotu") # This should be the name of the text model
end